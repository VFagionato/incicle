import styled from "styled-components";

export const Container = styled.div`
  display: block;
  flex-direction: column;
  background-color: #fff;
  padding: 10px;
  border-radius: 25px;
  margin-bottom: 20px;
`;

export const PostHeader = styled.div`
  display: flex;

  img {
    width: 40px;
    border-radius: 50%;
    border: 3px solid #304d5b;
    margin-right: 5px;
  }

  .header-info {
    align-self: flex-end;
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;

    .header-name {
      text-transform: capitalize;
      color: #316d78;
      font-weight: 600;
      font-size: 1rem;
      margin-bottom: 4px;
    }

    .header-restricted {
      font-size: 0.8rem;
      display: inline-block;
      display: flex;
      color: #4c4c4c;

      span {
        margin-right: 20px;
        display: flex;
        flex-wrap: nowrap;
        align-items: center;
      }

      svg {
        margin-right: 5px;
        align-self: flex-end;
        font-size: 0.9rem;
      }
    }
  }
`;

export const PostBody = styled.div`
  padding: 10px;
  display: flex;
  align-items: center;
  color: #4c4c4c;
`;

export const PostFooter = styled.div`
  display: flex;
  flex-direction: column;

  .post-info {
    width: 100%;
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid #e2e2e2;
    padding: 5px;
    margin-bottom: 5px;

    .like-status {
      display: flex;
      align-items: center;
      svg {
        margin-right: 5px;
        font-size: 1.2rem;
      }
    }

    span {
      margin-right: 10px;
      align-items: center;
    }
  }

  .post-buttons {
    width: 100%;
    display: flex;
    justify-content: space-around;
    font-size: 1.2rem;
  }
`;
