import { useEffect, useState } from "react";
import TimeAgo from "timeago-react";
import * as timeago from "timeago.js";
import { BiLike } from "react-icons/bi";
import { AiTwotoneLock, AiOutlineMessage } from "react-icons/ai";
import { GoGlobe } from "react-icons/go";
import { GiBackwardTime } from "react-icons/gi";
import { FaShareSquare } from "react-icons/fa";
import pt_br from "timeago.js/lib/lang/pt_BR";
import Button from "../Button";
import { Container, PostHeader, PostBody, PostFooter } from "./styles";

const Post = ({ data }) => {
  timeago.register("pt", pt_br);
  const [likes, setLikes] = useState(0);
  const [comments, setComments] = useState(0);
  const [shares, setShares] = useState(0);
  const [likeMessage, setLikesMessage] = useState("");
  const lockedMessage =
    'Essa publicação está configurada no modo privado "somente eu"';

  const handleRestriction = (restriction) => {
    if (restriction) {
      return (
        <>
          <AiTwotoneLock /> Somente eu
        </>
      );
    }
    return (
      <>
        <GoGlobe /> Público
      </>
    );
  };

  useEffect(() => {
    if (likes > 1) {
      setLikesMessage(`${likes} pessoas curtiram isso`);
      return;
    }
    if (likes === 1) {
      setLikesMessage(`${likes} pessoa curtiu isso`);
      return;
    }
    setLikesMessage(`Seja o primeiro a curtir`);
  }, [likes]);

  return (
    <>
      <Container>
        <PostHeader>
          <img src={data.picture} alt="" />
          <div className="header-info">
            <span className="header-name">{data.name}</span>
            <div className="header-restricted">
              <span>
                <GiBackwardTime />
                <TimeAgo datetime={data.timestamp} locale="pt" />
              </span>
              <span>{handleRestriction(data.restricted)}</span>
            </div>
          </div>
        </PostHeader>
        <PostBody>
          <span>{data.restricted ? lockedMessage : data.message}</span>
        </PostBody>
        <PostFooter>
          <div className="post-info">
            <span className="like-status">
              <BiLike /> {likeMessage}
            </span>
            <div>
              <span>{`${comments} comentários`}</span>
              <span>{`${shares} compartilhamentos`}</span>
            </div>
          </div>
          <div className="post-buttons">
            <Button
              icon={<BiLike />}
              value="curtir"
              onClick={() => setLikes(likes + 1)}
            />
            <Button
              icon={<AiOutlineMessage />}
              value="comentários"
              onClick={() => setComments(comments + 1)}
            />
            <Button
              icon={<FaShareSquare />}
              value="compartilhar"
              onClick={() => setShares(shares + 1)}
            />
          </div>
        </PostFooter>
      </Container>
    </>
  );
};

export default Post;
