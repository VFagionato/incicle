import { ButtonContainer, StyledButton } from "./style";

const Button = ({ icon, value, onClick }) => {
  return (
    <ButtonContainer>
      <span>{icon}</span>
      <StyledButton type="button" value={value} onClick={onClick} />
    </ButtonContainer>
  );
};

export default Button;
