import styled from "styled-components";

export const ButtonContainer = styled.div`
  position: relative;
  display: inline-block;

  input {
    text-indent: 10px;
  }

  span {
    position: absolute;
    top: 5px;
    left: 5px;
  }
`;

export const StyledButton = styled.input`
  border: none;
  border-radius: 10px;
  text-transform: capitalize;
  padding: 5px 20px;
  cursor: pointer;
  font-size: 1.2rem;
`;
