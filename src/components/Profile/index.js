import { MdPlace, MdWork } from "react-icons/md";
import { FaUniversity } from "react-icons/fa";
import {
  ProfileContainer,
  ProfileCover,
  InfoContainer,
  Image,
  NameStyled,
  UserNameStyled,
  PositionStyled,
  ExtraInfoContainer,
  GeoContainer,
} from "./styles";

const Profile = ({ data }) => {
  return (
    <>
      <ProfileContainer>
        <ProfileCover cover={data.cover ? data.cover : null} />
        <InfoContainer>
          <Image src={data.profilePic} alt="" />
          <NameStyled>{data.name}</NameStyled>
          <UserNameStyled>{data.username}</UserNameStyled>
          <PositionStyled>{data.position}</PositionStyled>
          <ExtraInfoContainer>
            <GeoContainer>
              <span>
                <MdPlace color="#94d8ed" />
                {`${data.city}/${data.state} - ${data.country}`}
              </span>
            </GeoContainer>
            <span className="last-info">
              <MdWork />
              {data.currentJob}
            </span>
            <span className="last-info">
              <FaUniversity />
              {data.education}
            </span>
          </ExtraInfoContainer>
        </InfoContainer>
      </ProfileContainer>
    </>
  );
};

export default Profile;
