import styled from "styled-components";
import defaultCover from "../../assets/image/cover.jpeg";

export const ProfileContainer = styled.div`
  width: 15rem;
  max-height: 350px;
  border-radius: 10px;
  background-color: white;
`;

export const ProfileCover = styled.div`
  width: 100%;
  height: 8rem;
  background: #fff url(${(props) => (props.cover ? props.cover : defaultCover)});
  background-repeat: no-repeat;
  background-size: cover;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  z-index: 3;

  &:before {
    width: 240px;
    height: 9px;
    background-color: #94d8ed;
    position: absolute;
    content: "";
    z-index: 0;
    top: 148px;
    border-radius: 18% 25% 316% 262%;
  }

  &:after {
    position: absolute;
    content: "";
    border-top: 12px solid transparent;
    border-left: 94px solid #4c9dc3;
    border-bottom: 21px solid transparent;
    top: 149px;
    z-index: 0;
  }
`;

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
  top: -50px;
  z-index: 10;
`;

export const Image = styled.img`
  width: 100px;
  border-radius: 50%;
  border: 4px solid #304d5b;
  margin-bottom: 10px;
`;

export const NameStyled = styled.span`
  color: #316d78;
  text-transform: capitalize;
  font-weight: 700;
  font-size: 1.2rem;
`;

export const UserNameStyled = styled.span`
  font-weight: 900;
  font-size: 0.9rem;
  margin-bottom: 5px;
`;

export const PositionStyled = styled.span`
  font-size: 0.8rem;
  font-weight: 100;
  margin-bottom: 10px;
`;

export const ExtraInfoContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  color: #676666;

  span {
    font-size: 0.9rem;
    margin-bottom: 5px;
  }

  svg {
    margin-right: 5px;
  }
`;

export const GeoContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-bottom: 10px;
`;
