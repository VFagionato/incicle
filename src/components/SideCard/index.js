import { Container } from "./styles";

const SideCard = ({ icon, title, time, text }) => {
  const handleTime = (time) => {
    if (time) {
      const options = {
        weekday: "short",
        month: "short",
        day: "numeric",
      };

      let timeString = time.toLocaleDateString("pt-BR", options);

      return timeString.replace(".", "");
    }
  };

  return (
    <>
      <Container>
        <div className="card-header">
          <span>{icon}</span>
          <span>{title}</span>
        </div>
        <div className="card-time">
          <span>{handleTime(time)}</span>
        </div>
        <div className="card-body">
          <span>{text}</span>
        </div>
      </Container>
    </>
  );
};

export default SideCard;
