import styled from "styled-components";

export const Container = styled.div`
  background-color: #fff;
  border-radius: 25px;
  margin-bottom: 20px;
  padding: 10px 0;

  .card-header {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.3rem;
    color: #94d8ed;
    padding: 10px 0;

    svg {
      margin-right: 10px;
    }
  }

  .card-time {
    background-color: #ececec;
    min-height: 2px;
    padding: 2px 5px;
  }

  .card-body {
    padding: 10px;
    color: #4c4c4c;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
