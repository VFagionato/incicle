/* eslint-disable react-hooks/exhaustive-deps */
import Profile from "./components/Profile";
import Post from "./components/Posts";
import SideCard from "./components/SideCard";
import imageProfile from "./assets/image/garfield.png";
import coverDemo from "./assets/image/cover2.jpeg";
import postPic from "./assets/image/profile-second.jpeg";
import { FaBirthdayCake } from "react-icons/fa";
import { GiGears } from "react-icons/gi";
import { useEffect, useState } from "react";
import styled from "styled-components";

const App = () => {
  let profileData = {
    profilePic: imageProfile,
    name: "viktor fagionato",
    username: "@viktor_fgnt",
    position: "Full Stack Developer",
    city: "Santo André",
    state: "São Paulo",
    country: "Brasil",
    currentJob: "Huge-Networks",
    education: "Kenzie Academy",
    cover: coverDemo,
  };

  let postData = [
    {
      picture: postPic,
      name: "patricia santos",
      timestamp: 1627830154000,
      restricted: true,
      message: "teste123",
    },
    {
      picture: postPic,
      name: "patricia santos",
      timestamp: 1627167880000,
      restricted: false,
      message: "kkkkkk",
    },
  ];

  const [info, setInfo] = useState(profileData);

  useEffect(() => {
    setTimeout(() => {
      setInfo({
        ...profileData,
        name: "Kenedy Morais",
        username: "@kenedymorais",
        position: "Front-end Developer",
        city: "Rio Branco",
        state: "Acre",
        currentJob: "inCicle",
        education: "Uninorte",
        cover: null,
      });
    }, 5000);
  }, []);

  return (
    <Container>
      <Profile data={info} />
      <PostContainer>
        {postData.map((data, index) => (
          <Post key={index} data={data} />
        ))}
      </PostContainer>
      <SideContainer>
        <SideCard
          icon={<FaBirthdayCake size={30} />}
          title="Aniversáriantes"
          text="Nenhum dos seus amigos comemora aniversário hoje"
        />
        <SideCard
          icon={<GiGears size={30} />}
          time={new Date()}
          title="Pendências"
          text="Você não possui nenhuma pendência para hoje"
        />
      </SideContainer>
    </Container>
  );
};

export default App;

const Container = styled.div`
  margin-top: 20px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const PostContainer = styled.div`
  width: 50%;
`;

const SideContainer = styled.div`
  width: 25%;
`;
